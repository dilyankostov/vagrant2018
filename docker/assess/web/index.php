<?php
# Script to show data from the authors table
# http://myserver/index.php?dbsrv=abc.xyz.com&dbuser=steve&dbpass=secret123

$servername = $_GET["dbsrv"];
$username = $_GET["dbuser"];
$password = $_GET["dbpass"];
$dbname = "pubs";

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT au_id, au_fname, au_lname FROM authors";
$result = $conn->query($sql);
?>
<table border="2">
<tr><th>ID</th><th>Firstname</th><th>Lastname</th></tr>
<?php
if ($result->num_rows > 0) {
    // output data of each row
    while($row = $result->fetch_assoc()) {
      echo "<tr><td>" . $row["au_id"] . "</td><td>" . $row["au_fname"] . "</td><td>" . $row["au_lname"] . "</td></tr>";
        #echo "id: " . $row["au_id"]. " - Name: " . $row["au_fname"]. " " . $row["au_lname"]. "<br>";
    }
} else {
    echo "0 results";
}
$conn->close();
?>

